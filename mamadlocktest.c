#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "date.h"

void fakeProc()
{
    if (fork() == 0)
    {
        test_mamadlock();
    }
}

int main(int argc, char *argv[])
{
    for (int i = 0; i < 5; i++)
        fakeProc();
    exit();
}