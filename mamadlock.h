// HERE 4
struct mamadlock
{
  uint locked;
  struct spinlock lk;

  char *name;
  int pid;

  struct proc *queue[NPROC];
  int lastIndex;
  int chosenProcPid;
  int queueNum;
};