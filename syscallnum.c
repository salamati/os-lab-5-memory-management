#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "date.h"

void fakeProc()
{
    if (fork() == 0)
    {
        for (int i = 0; i < 10; i++)
            set_edx(5);
        exit();
    }
}

int main(int argc, char *argv[])
{
    for (int i = 0; i < 5; i++)
        fakeProc();
    // wait 2 seconds
    sleep(200);
    syscalls_num();
    exit();
}